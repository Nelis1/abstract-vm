# **************************************************************************** #
#                                                                              #
#                                                         :::      ::::::::    #
#    Makefile                                           :+:      :+:    :+:    #
#                                                     +:+ +:+         +:+      #
#    By: cnolte <cnolte@42.fr>                      +#+  +:+       +#+         #
#                                                 +#+#+#+#+#+   +#+            #
#    Created: 2018/05/29 23:46:32 by cnolte            #+#    #+#              #
#    Updated: 2018/07/21 18:41:48 by cnolte           ###   ########.fr        #
#                                                                              #
# **************************************************************************** #

NAME := avm

SRCS := $(wildcard source/*.cpp) $(wildcard source/classes/*.cpp) $(wildcard source/functions/*.cpp)
OBJS := $(SRCS:.cpp=.o)

CXX := clang++ -std=c++11
CXXFLAGS := -Wall -Werror -Wextra

INC_DIRS = include/
CXXFLAGS += $(addprefix -I, $(INC_DIRS))

$(NAME): $(OBJS)
	@$(CXX) $(OBJS) -o $@
	@echo [INFO] $@ "compiled successfully."

all: $(NAME)

clean:
	@rm -f $(OBJS)
	@echo "[INFO] Objects removed!"

fclean: clean
	@rm -f $(NAME)
	@echo "[INFO] $(NAME) removed!"
	@rm -rf input.tmp

re: fclean all

.PHONY: all clean fclean re
