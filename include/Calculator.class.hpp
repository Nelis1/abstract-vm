/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   Calculator.class.hpp                               :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: cnolte <cnolte@42.fr>                      +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2018/06/11 11:20:47 by cnolte            #+#    #+#             */
/*   Updated: 2018/07/21 18:42:36 by cnolte           ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#ifndef CALCULATOR_CLASS_HPP
# define CALCULATOR_CLASS_HPP

# include <iostream>
# include "Factory.class.hpp"
# include "enum.hpp"

class Calculator
{
	public:
		//start canonical form
		Calculator(void);
		Calculator(int const n);
		Calculator(Calculator const & src);
		~Calculator(void);

		Calculator	&operator=(Calculator const &rhs);
		//end canonical form

		void	addToStack(commands cmd, std::string value);
		void	popFromStack(void);
		void	addOperator(void);
		void	subOperator(void);
		void	mulOperator(void);
		void	divOperator(void);
		void	modOperator(void);
		void	dumpCommand(void);
		void	assertCommand(const eOperandType type, const std::string &value);
		void	printCommand(void) const;
	private:
		Factory								factory;
		std::vector<const IOperand *>		operand_table;
};

std::ostream	&operator<<(std::ostream &out, Calculator const &value);

#endif
