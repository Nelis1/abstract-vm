/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   Exceptions.hpp                                     :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: cnolte <cnolte@42.fr>                      +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2018/07/16 16:21:59 by cnolte            #+#    #+#             */
/*   Updated: 2018/07/21 18:35:08 by cnolte           ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#ifndef EXCEPTIONS_HPP
# define EXCEPTIONS_HPP
# include <iostream>

namespace Exceptions {
	class NoExit: public std::exception {
		public:
			const char	*what(void) const throw();
	};

	class EmptyStack: public std::exception {
		public:
			const char	*what(void) const throw();
	};

	class AssertValueFailure: public std::exception {
		public:
			const char	*what(void) const throw();
	};

	class AssertTypeFailure: public std::exception {
		public:
			const char	*what(void) const throw();
	};

	class AssertOnEmptyStack: public std::exception {
		public:
			const char	*what(void) const throw();
	};

	class StackFailure: public std::exception {
		public:
			const char	*what(void) const throw();
	};

	class DivisionByZero: public std::exception {
		public:
			const char	*what(void) const throw();
	};

	class UnknownInstruction: public std::exception {
		public:
			const char	*what(void) const throw();
	};

	class ValueOverflow: public std::exception {
		public:
			const char	*what(void) const throw();
	};

	class ValueUnderflow: public std::exception {
		public:
			const char	*what(void) const throw();
	};

	class PopOnEmptyStack: public std::exception {
		public:
			const char	*what(void) const throw();
	};

	class OperationOnEmptyStack: public std::exception {
		public:
			const char	*what(void) const throw();
	};

	class OperationOnTooSmallStack: public std::exception {
		public:
			const char	*what(void) const throw();
	};

	class PrintOnEmptyStack: public std::exception {
		public:
			const char	*what(void) const throw();
	};

	class PrintNonAscii: public std::exception {
		public:
			const char	*what(void) const throw();
	};

	class FloatingPointException: public std::exception {
		public:
			const char	*what(void) const throw();
	};

	class ConversionNotPossible: public std::exception {
		public:
			const char	*what(void) const throw();
	};

	class ModuloOnFloatingPoint: public std::exception
	{
		public:
			const char	*what(void) const throw();
	};

	class FileDoesNotExist: public std::exception
	{
		private:
			std::string _filename;
		public:
			virtual const char* what() const throw();
		FileDoesNotExist(const std::string filename)
		{
			_filename = filename;
		}
	};
};

#endif
