/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   Factory.class.hpp                                  :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: cnolte <cnolte@42.fr>                      +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2018/06/11 11:20:47 by cnolte            #+#    #+#             */
/*   Updated: 2018/07/21 18:42:47 by cnolte           ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#ifndef FACTORY_CLASS_HPP
# define FACTORY_CLASS_HPP

# include <vector>
# include <cmath>
# include "IOperand.hpp"
# include "eOperandType.hpp"


class Factory {
	typedef const IOperand *(Factory::*OperandFunc)(const std::string &) const;

	std::vector<OperandFunc> stack;

	Factory(const Factory &rhs);
	Factory	&operator=(const Factory &rhs) const;

	public:
		Factory(void);
		~Factory(void);

		const IOperand*	createOperand(const eOperandType type, const std::string &value) const;

	private:
		const IOperand	*createInt8(const std::string &value) const;
		const IOperand	*createInt16(const std::string &value) const;
		const IOperand	*createInt32(const std::string &value) const;
		const IOperand	*createFloat(const std::string &value) const;
		const IOperand	*createDouble(const std::string &value) const;
};

#endif
