/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   IOperand.hpp                                       :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: cnolte <cnolte@42.fr>                      +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2018/07/14 12:55:41 by cnolte            #+#    #+#             */
/*   Updated: 2018/07/20 14:36:33 by cnolte           ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#ifndef IOPERAND_HPP
# define IOPERAND_HPP
# include <iostream>
# include "eOperandType.hpp"

class IOperand {

	public:
		//GETTERS
		virtual int					getPrecision(void) const = 0;
		virtual eOperandType		getType(void) const = 0;

		//OPERATOR OVERLOAD
		virtual IOperand const	*operator+(const IOperand &rhs) const = 0;
		virtual IOperand const	*operator-(const IOperand &rhs) const = 0;
		virtual IOperand const	*operator*(const IOperand &rhs) const = 0;
		virtual IOperand const	*operator/(const IOperand &rhs) const = 0;
		virtual IOperand const	*operator%(const IOperand &rhs) const = 0;

		//TO STRING METHOD
		virtual std::string const & toString( void ) const = 0;

		//DESTRUCTOR
		virtual ~IOperand(void) {}
};

#endif
