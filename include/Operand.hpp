/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   Operand.hpp                                        :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: dwilliam <dwilliam@42.fr>                  +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2018/07/20 16:28:13 by dwilliam          #+#    #+#             */
/*   Updated: 2018/07/21 17:34:53 by dwilliam         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#ifndef OPERAND_HPP

# define OPERAND_HPP

# include "IOperand.hpp"
# include "Factory.class.hpp"
# include "Exceptions.hpp"
# include <climits>
# include "avm.header.hpp"

template <typename T>
class Operand: public IOperand
{
	public:
		Operand(void);
		Operand(eOperandType type, const T value);
		~Operand(void);
		Operand(const Operand &rhs) : _type(rhs.getType()), _value(rhs.getValue()) {};
		Operand & operator=(const Operand &rhs) { return (*new Operand(rhs));}

		eOperandType getType(void) const;
		std::string const & toString( void ) const;
		IOperand const	*operator+(const IOperand &rhs) const;
		IOperand const	*operator-(const IOperand &rhs) const;
		IOperand const	*operator*(const IOperand &rhs) const;
		IOperand const	*operator/(const IOperand &rhs) const;
		IOperand const	*operator%(const IOperand &rhs) const;

		int		getPrecision(void) const;
		T		getValue() const;

	private:
		eOperandType	_type;
		int				_getPrecision;
		std::string		_str;
		T				_value;
		Factory			_factory;
};

template <typename T>
Operand<T>::Operand(void)
{

}

template <typename T>
Operand<T>::Operand(eOperandType type, const T value)
{
	int tmp_int;
	double_t tmp_double;
	switch (type)
	{
		case Int8:
		case Int16:
		case Int32:
			tmp_int = static_cast<int>(value);
			this->_str = std::to_string(tmp_int);
			break ;
		case Float:
		case Double:
			tmp_double = static_cast<double_t>(value);
			this->_str = std::to_string(tmp_double);
			break ;
		default:
			break ;
	}
	_type = type;
	_value = value;
}

template <typename T>
Operand<T>::~Operand(void)
{

}

template <typename T>
eOperandType Operand<T>::getType(void) const
{
	return this->_type;
}

template <typename T>
std::string const &Operand<T>::toString( void ) const
{
	return this->_str;
}

template <typename T>
int Operand<T>::getPrecision(void) const
{
	return this->_type;
}

template <typename T>
T Operand<T>::getValue() const
{
	return this->_value;
}

template <typename T>
IOperand const *Operand<T>::operator+(const IOperand &rhs) const
{
	eOperandType	type;
	int				lhs = 0;
	double			ret = 0;

	if (!testOverUnderFlow(_type, std::stod(rhs.toString()) + _value))
		return NULL;
	else if (_type == rhs.getPrecision())
	{
		return (new Operand<T>(_type, std::stoi(rhs.toString()) + _value));
	}
	else
	{
		if (_type > rhs.getPrecision())
			type = _type;
		else
			type = rhs.getType();
		switch (type) {
			case Int8:
			case Int16:
			case Int32:
				lhs = std::stoi(rhs.toString()) + _value;
				break ;
			case Float:
			case Double:
				ret = std::stod(rhs.toString()) + _value;
				break ;
			case Error:
				break ;
		}
		return (_factory.createOperand(type, std::to_string((lhs) ? lhs : ret)));
	}
	return (NULL);
}

template <typename T>
IOperand const *Operand<T>::operator-(const IOperand &rhs) const
{
	eOperandType	type;
	int				lhs = 0;
	double			ret = 0;
	if (!testOverUnderFlow(_type, std::stod(rhs.toString()) - _value))
	return NULL;
	if (_type == rhs.getPrecision())
	{
		return (new Operand<T>(_type, std::stoi(rhs.toString()) - _value));
	}
	else
	{
		if (_type > rhs.getPrecision())
			type = _type;
		else
			type = rhs.getType();
		switch (type) {
			case Int8:
			case Int16:
			case Int32:
				lhs = std::stoi(rhs.toString()) - _value;
				break ;
			case Float:
			case Double:
				ret = std::stod(rhs.toString()) - _value;
				break ;
			case Error:
				break ;
		}
		return (_factory.createOperand(type, std::to_string((lhs) ? lhs : ret)));
	}
	return (NULL);
}

template <typename T>
IOperand const *Operand<T>::operator*(const IOperand &rhs) const
{
	eOperandType	type;
	int				lhs = 0;
	double			ret = 0;

	if (!testOverUnderFlow(_type, std::stod(rhs.toString()) * _value))
	return NULL;
	if (_type == rhs.getPrecision())
	{
		return (new Operand<T>(_type, std::stoi(rhs.toString()) * _value));
	}
	else
	{
		if (_type > rhs.getPrecision())
			type = _type;
		else
			type = rhs.getType();
		switch (type) {
			case Int8:
			case Int16:
			case Int32:
				lhs = std::stoi(rhs.toString()) * _value;
				break ;
			case Float:
			case Double:
				ret = std::stod(rhs.toString()) * _value;
				break ;
			case Error:
				break ;
		}
		return (_factory.createOperand(type, std::to_string((lhs) ? lhs : ret)));
	}
	return (NULL);
}

template <typename T>
IOperand const *Operand<T>::operator/(const IOperand &rhs) const
{
	eOperandType	type;
	int				lhs = 0;
	double			ret = 0;

	if (!testOverUnderFlow(_type, std::stod(rhs.toString()) / _value))
		return NULL;
	if (_type == rhs.getPrecision())
	{
		return (new Operand<T>(_type, std::stoi(rhs.toString()) / _value));
	}
	else
	{
		if (_type > rhs.getPrecision())
			type = _type;
		else
			type = rhs.getType();
		switch (type) {
			case Int8:
			case Int16:
			case Int32:
				lhs = std::stoi(rhs.toString()) / _value;
				break ;
			case Float:
			case Double:
				ret = std::stod(rhs.toString()) / _value;
				break ;
			case Error:
				break ;
		}
		return (_factory.createOperand(type, std::to_string((lhs) ? lhs : ret)));
	}
	return (NULL);

}

template <typename T>
IOperand const *Operand<T>::operator%(const IOperand &rhs) const
{
	eOperandType	type;
	int				lhs = 0;
	double			ret = 0;

	if (!testOverUnderFlow(_type, static_cast<long int>(std::stoi(rhs.toString())) % static_cast<long int>(_value)))
		return NULL;
	if (_type == rhs.getPrecision())
	{
		return (new Operand<T>(_type, static_cast<long int>(std::stoi(rhs.toString())) % static_cast<long int>(_value)));
	}
	else
	{
		if (_type > rhs.getPrecision())
			type = _type;
		else
			type = rhs.getType();
		switch (type) {
			case Int8:
			case Int16:
			case Int32:
				lhs = static_cast<long int>(std::stoi(rhs.toString())) % static_cast<long int>(_value);
				break ;
			case Float:
			case Double:
				throw Exceptions::ModuloOnFloatingPoint();
				break ;
			case Error:
				break ;
		}
		return (_factory.createOperand(type, std::to_string((lhs) ? lhs : ret)));
	}
	return (NULL);

}

#endif
