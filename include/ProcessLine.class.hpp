/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ProcessLine.class.hpp                              :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: cnolte <cnolte@42.fr>                      +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2018/06/11 11:20:47 by cnolte            #+#    #+#             */
/*   Updated: 2018/07/17 07:23:41 by cnolte           ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#ifndef PROCESSLINE_CLASS_HPP
# define PROCESSLINE_CLASS_HPP

# include <iostream>
# include "Calculator.class.hpp"
# include "enum.hpp"
# include "Exceptions.hpp"

class ProcessLine
{
	public:
		//start canonical form
		ProcessLine(void);
		ProcessLine(int const n);
		ProcessLine(ProcessLine const & src);
		~ProcessLine(void);

		ProcessLine	&operator=(ProcessLine const &rhs);
		//end canonical form

		void	process_line(std::string line);
	private:
		Calculator	cal;
};

std::ostream	&operator<<(std::ostream &out, ProcessLine const &value);

#endif
