/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   avm.header.hpp                                     :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: cnolte <cnolte@42.fr>                      +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2018/07/05 15:49:33 by cnolte            #+#    #+#             */
/*   Updated: 2018/07/20 16:43:55 by cnolte           ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#ifndef AVM_HEADER_HPP
# define AVM_HEADER_HPP

# include <iostream>
# include "ProcessLine.class.hpp"

//-------------------------------- str functions -------------------------------
std::string	str_to_lower(std::string str);
char		*char_str_to_lower(char *str);
bool		hasEnding(std::string const &fullString, std::string const &ending);

//-------------------------------- dir functions --------------------------------
std::string	get_avm_file(int selection);
bool		print_avm_files(void);

//---------------------------------- processing ---------------------------------
void		process_file(std::string filename, ProcessLine process_line);

//-------------------------- over & underflow checks ----------------------------
bool	testOverUnderFlow(eOperandType type, double d);

#endif
