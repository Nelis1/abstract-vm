/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   eOperandType.hpp                                   :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: cnolte <cnolte@42.fr>                      +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2018/07/14 12:58:19 by cnolte            #+#    #+#             */
/*   Updated: 2018/07/20 16:37:19 by cnolte           ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#ifndef EOPERANDTYPE_HPP
# define EOPERANDTYPE_HPP
# include <iostream>

enum eOperandType
{
	Int8,
	Int16,
	Int32,
	Float,
	Double,
	Error
};

#endif
