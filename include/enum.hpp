/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   enum.hpp                                           :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: cnolte <cnolte@42.fr>                      +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2018/07/16 10:44:14 by cnolte            #+#    #+#             */
/*   Updated: 2018/07/16 11:43:25 by cnolte           ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#ifndef ENUM_HPP
# define ENUM_HPP

enum class commands {push8, push16, push32, pushDouble, pushFloat, pop, dump, assert8,
					assert16, assert32, assertDouble, assertFloat, add, sub, mul, div,
					mod, print, exit, NONE};

#endif
