//https://www.youtube.com/watch?v=v_8rdQjOuxw
#include <regex>
#include <iostream>

int	main(void)
{
	std::string str;

	while (true)
	{
		std::cin >> str;
		std::regex e("abc", std::regex_constants::icase);

		bool	match = regex_match(str, e);

		std::cout << (match? "Matched" : "Not matched") << std::endl << std::endl;
	}
	return (0);
}
