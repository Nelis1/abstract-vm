// https://www.youtube.com/watch?v=UG0smS0nFV4
#include <iostream>

using namespace std;

class Project
{
	public:
		virtual void AddFile() = 0;
};

class UnisysProject : public Project
{
	public:
		virtual void AddFile()
		{
			cout << "UnisysProject::AddFile()" << endl;
		}
};

class IBMProject : public Project
{
	public:
		virtual void AddFile()
		{
			cout << "IBMProject::AddFile()" << endl;
		}
};

class ProjectCreator
{
	private:
		Project *project;
	public:
		virtual Project *CreateProject() = 0;
		Project *NewProject()
		{
			cout << "ProjectCreator::NewProject()" << endl;
			project = CreateProject();
			return (project);
		}
};

class UnisysProjectCreator : public ProjectCreator
{
	public:
		virtual Project *CreateProject()
		{
			cout << "UnisysProjectCreator::CreateProject()" << endl;
			return (new UnisysProject);
		}
};

class IBMProjectCreator : public ProjectCreator
{
	public:
		virtual Project *CreateProject()
		{
			cout << "IBMProjectCreator::CreateProject()" << endl;
			return (new IBMProject);
		}
};

int main(void)
{
	Project *proj;
	ProjectCreator *projCreator;

	projCreator = new UnisysProjectCreator();
	proj = projCreator->NewProject();
	proj->AddFile();

	projCreator = new IBMProjectCreator();
	proj = projCreator->NewProject();
	proj->AddFile();

	return (0);
}
