/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   Calculator.class.cpp                                   :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: cnolte <cnolte@42.fr>                      +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2018/06/11 11:26:34 by cnolte            #+#    #+#             */
/*   Updated: 2018/06/11 12:39:44 by cnolte           ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include <iostream>
#include "Calculator.class.hpp"
#include "Operand.hpp"
#include "Exceptions.hpp"

//start canonical form
Calculator::Calculator(void)
{
	return ;
}

Calculator::Calculator(Calculator const & src)
{
	*this = src;

	return ;
}

Calculator::~Calculator(void)
{
	return ;
}

Calculator	&Calculator::operator=(Calculator const &rhs)
{
	return (*new Calculator(rhs));
}
//end canonical form

void	Calculator::addToStack(commands cmd, std::string value)
{
	eOperandType	operand_type = Error;

	switch (cmd)
	{
		case commands::push8:
			operand_type = Int8;
			break ;
		case commands::push16:
			operand_type = Int16;
			break ;
		case commands::push32:
			operand_type = Int32;
			break ;
		case commands::pushFloat:
			operand_type = Float;
			break ;
		case commands::pushDouble:
			operand_type = Double;
			break ;
		case commands::NONE:
			throw Exceptions::UnknownInstruction();
			break ;
		default:	//because not all enums is processed
			break ;
	}
	operand_table.push_back(factory.createOperand(operand_type, value));
}

void	Calculator::popFromStack(void)
{
	if (operand_table.empty())
		throw Exceptions::PopOnEmptyStack();
	else
		operand_table.pop_back();
}

void	Calculator::addOperator(void)
{
	const IOperand	*o1 = NULL;
	const IOperand	*o2 = NULL;

	if (operand_table.size() == 0)
		throw Exceptions::OperationOnEmptyStack();
	else if (operand_table.size() < 2)
		throw Exceptions::OperationOnTooSmallStack();
	o1 = *(operand_table.rbegin());
	operand_table.pop_back();
	o2 = *(operand_table.rbegin());
	operand_table.pop_back();
	operand_table.push_back(*o1 + *o2);
}

void	Calculator::subOperator(void)
{
	const IOperand	*o1 = NULL;
	const IOperand	*o2 = NULL;

	if (operand_table.size() == 0)
		throw Exceptions::OperationOnEmptyStack();
	else if (operand_table.size() < 2)
		throw Exceptions::OperationOnTooSmallStack();
	o1 = *(operand_table.rbegin());
	operand_table.pop_back();
	o2 = *(operand_table.rbegin());
	operand_table.pop_back();
	operand_table.push_back(*o1 - *o2);
}

void	Calculator::mulOperator(void)
{
	const IOperand	*o1 = NULL;
	const IOperand	*o2 = NULL;

	if (operand_table.size() == 0)
		throw Exceptions::OperationOnEmptyStack();
	else if (operand_table.size() < 2)
		throw Exceptions::OperationOnTooSmallStack();
	o1 = *(operand_table.rbegin());
	operand_table.pop_back();
	o2 = *(operand_table.rbegin());
	operand_table.pop_back();
	operand_table.push_back(*o1 * *o2);
}

void	Calculator::divOperator(void)
{
	const IOperand	*o1 = NULL;
	const IOperand	*o2 = NULL;

	if (operand_table.size() < 2)
		throw Exceptions::OperationOnEmptyStack();
	o1 = *(operand_table.rbegin());
	operand_table.pop_back();
	o2 = *(operand_table.rbegin());
	operand_table.pop_back();
	operand_table.push_back(*o1 / *o2);
}

void	Calculator::modOperator(void)
{
	const IOperand	*o1 = NULL;
	const IOperand	*o2 = NULL;

	if (operand_table.size() == 0)
		throw Exceptions::OperationOnEmptyStack();
	else if (operand_table.size() < 2)
		throw Exceptions::OperationOnTooSmallStack();
	o1 = *(operand_table.rbegin());
	operand_table.pop_back();
	o2 = *(operand_table.rbegin());
	operand_table.pop_back();
	operand_table.push_back(*o1 % *o2);
}

void	Calculator::dumpCommand(void)
{
	std::vector<const IOperand *>::const_reverse_iterator	it;
	std::vector<const IOperand *>::const_reverse_iterator	ite;

	it = operand_table.rbegin();
	ite = operand_table.rend();
	while (it != ite) {
		std::cout << (*it)->toString() << std::endl;
		it++;
	}
}

void	Calculator::assertCommand(const eOperandType type, const std::string &value)
{
	if (!operand_table.size())
		throw Exceptions::AssertOnEmptyStack();
	const IOperand	&operand = **(operand_table.rbegin());
	if (operand.getType() != type)
		throw Exceptions::AssertTypeFailure();
	if (value != operand.toString())
		throw Exceptions::AssertValueFailure();
}

void	Calculator::printCommand(void) const
{
	const IOperand		*ioperand = NULL;
	const Operand<char>	*operand = NULL;

	if (operand_table.empty())
		throw Exceptions::PrintOnEmptyStack();
	ioperand = *(operand_table.rbegin());
	if (ioperand->getType() != Int8)
		throw Exceptions::PrintNonAscii();
	operand = static_cast<const Operand<char> *>(ioperand);
	std::cout << operand->getValue() << std::endl;
}
