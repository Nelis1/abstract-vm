/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   Exceptions.cpp                                     :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: cnolte <cnolte@42.fr>                      +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2018/07/16 16:28:27 by cnolte            #+#    #+#             */
/*   Updated: 2018/07/21 18:35:22 by cnolte           ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "Exceptions.hpp"

const char	*Exceptions::NoExit::what(void) const throw()
{
	return ("No \"exit\" command found.");
}

const char	*Exceptions::EmptyStack::what(void) const throw()
{
	return ("Stack is empty.");
}

const char	*Exceptions::AssertValueFailure::what(void) const throw()
{
	return ("Assert value does not match with top of stack.");
}

const char	*Exceptions::AssertTypeFailure::what(void) const throw()
{
	return ("Assert type does not match with top of stack.");
}

const char	*Exceptions::AssertOnEmptyStack::what(void) const throw()
{
	return ("Assert on empty stack.");
}

const char	*Exceptions::StackFailure::what(void) const throw()
{
	return ("Stack size is less than 2.");
}

const char	*Exceptions::DivisionByZero::what(void) const throw()
{
	return ("Division by zero.");
}

const char	*Exceptions::UnknownInstruction::what(void) const throw()
{
	return ("Unknown instruction found.");
}

const char	*Exceptions::ValueOverflow::what(void) const throw()
{
	return ("Overflow of a value.");
}

const char	*Exceptions::ValueUnderflow::what(void) const throw()
{
	return ("Underflow on a value.");
}

const char	*Exceptions::PopOnEmptyStack::what(void) const throw()
{
	return ("Pop called on an empty stack.");
}

const char	*Exceptions::OperationOnEmptyStack::what(void) const throw()
{
	return ("Operation on an empty stack.");
}

const char	*Exceptions::OperationOnTooSmallStack::what(void) const throw()
{
	return ("Operation on a stack with only one value. Minimum 2 required.");
}

const char	*Exceptions::PrintOnEmptyStack::what(void) const throw()
{
	return ("Print on an empty stack.");
}

const char	*Exceptions::PrintNonAscii::what(void) const throw()
{
	return ("Print non ASCII.");
}

const char	*Exceptions::FloatingPointException::what(void) const throw()
{
	return ("Division/Modulo by zero.");
}

const char	*Exceptions::ConversionNotPossible::what(void) const throw()
{
	return ("Conversion not possible.");
}

const char	*Exceptions::ModuloOnFloatingPoint::what(void) const throw()
{
	return ("Can't make a modulo on floating point.");
}

const char *Exceptions::FileDoesNotExist::what(void) const throw()
{
	std::string ret_msg = "Unable to open file \"" + _filename + "\".";
	return (ret_msg.c_str());
}
