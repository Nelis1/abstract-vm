/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   Factory.class.cpp                                   :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: cnolte <cnolte@42.fr>                      +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2018/06/11 11:26:34 by cnolte            #+#    #+#             */
/*   Updated: 2018/06/11 12:39:44 by cnolte           ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include <iostream>
#include "Factory.class.hpp"
#include "Operand.hpp"

Factory::Factory(void)
{
	this->stack.push_back(&Factory::createInt8);
	this->stack.push_back(&Factory::createInt16);
	this->stack.push_back(&Factory::createInt32);
	this->stack.push_back(&Factory::createFloat);
	this->stack.push_back(&Factory::createDouble);
	return ;
}

Factory::Factory(Factory const & src)
{
	*this = src;

	return ;
}

Factory::~Factory(void)
{
	return ;
}

Factory	&Factory::operator=(const Factory &rhs) const
{
	return (*new Factory(rhs));
}

const IOperand *Factory::createOperand(const eOperandType type, std::string const &value) const
{
	return (this->*(this->stack[type]))(value);
}

const IOperand	*Factory::createInt8(const std::string &value) const
{
	return (new Operand<char>(Int8, stoi(value)));
}

const IOperand	*Factory::createInt16(const std::string &value) const
{
	return (new Operand<short>(Int16, stoi(value)));
}

const IOperand	*Factory::createInt32(const std::string &value) const
{
	return (new Operand<int>(Int32, stoi(value)));
}

const IOperand	*Factory::createFloat(const std::string &value) const
{
	return (new Operand<float>(Float, stof(value)));
}

const IOperand	*Factory::createDouble(const std::string &value) const
{
	return (new Operand<double>(Double, stod(value)));
}
