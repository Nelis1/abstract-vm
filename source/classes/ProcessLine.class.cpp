/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ProcessLine.class.cpp                              :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: cnolte <cnolte@42.fr>                      +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2018/06/11 11:26:34 by cnolte            #+#    #+#             */
/*   Updated: 2018/07/21 18:40:27 by cnolte           ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include <iostream>
#include "ProcessLine.class.hpp"
#include "avm.header.hpp"

//start canonical form
ProcessLine:: ProcessLine(void)
{
	return ;
}

ProcessLine::ProcessLine(ProcessLine const & src)
{
	*this = src;

	return ;
}

ProcessLine::~ProcessLine(void)
{
	return ;
}

ProcessLine	&ProcessLine::operator=(ProcessLine const &rhs)
{
	(void)rhs;
	return *this;
}
//end canonical form

bool	is_digit_and_dot(const std::string str)
{
	return str.find_first_not_of("-0123456789.") == std::string::npos;
}

bool	is_digits_only(const std::string str)
{
	return str.find_first_not_of("-0123456789") == std::string::npos;
}

bool	verify_if_digit(eOperandType type, std::string line)
{
	std::string value(std::find(line.begin(), line.end(), '(') + 1, std::find(line.begin(), line.end(), ')'));
	if ((type >= (eOperandType)0) && type <= (eOperandType)2)
	{
		if (is_digits_only(value))
		{
			double	testint = std::stod(value);
			if ((std::count(value.begin(), value.end(), '-') == 1) && value.find("-") == 0)
				return (testOverUnderFlow(type, testint));
			else if (std::count(value.begin(), value.end(), '-') == 0)
				return (testOverUnderFlow(type, testint));
		}
	}
	else if ((type >= (eOperandType)3) && type <= (eOperandType)4)
	{
		if (is_digits_only(value))
		{
			double	testDoubleOrFloat = std::stod(value);
			if ((std::count(value.begin(), value.end(), '-') == 1) && value.find("-") == 0)
				return (testOverUnderFlow(type, testDoubleOrFloat));
			else if (std::count(value.begin(), value.end(), '-') == 0)
				return (testOverUnderFlow(type, testDoubleOrFloat));
		}
		else if (is_digit_and_dot(value))
		{
			double	testDoubleOrFloat = std::stod(value);
			if ((std::count(value.begin(), value.end(), '-') == 1) && value.find("-") == 0)
			{
				if (std::count(value.begin(), value.end(), '.') == 1)
					if ((value.find(".") < (value.length() - 1)) && (value.find(".") > 1))
						return (testOverUnderFlow(type, testDoubleOrFloat));
			}
			else if (std::count(value.begin(), value.end(), '-') == 0)
				if (std::count(value.begin(), value.end(), '.') == 1)
					if ((value.find(".") < (value.length() - 1)) && (value.find(".") > 0))
						return (testOverUnderFlow(type, testDoubleOrFloat));
		}
	}
	return (false);
}

commands	test_assert(std::string line)
{
	if (!line.find("assert int8("))
		return (verify_if_digit(Int8, line) ? commands::assert8 : commands::NONE);
	else if (!line.find("assert int16("))
		return (verify_if_digit(Int16, line) ? commands::assert16 : commands::NONE);
	else if (!line.find("assert int32("))
		return (verify_if_digit(Int32, line) ? commands::assert32 : commands::NONE);
	else if (!line.find("assert float("))
		return (verify_if_digit(Float, line) ? commands::assertFloat : commands::NONE);
	else if (!line.find("assert double("))
		return (verify_if_digit(Double, line) ? commands::assertDouble : commands::NONE);
	return (commands::NONE);
}

commands	test_push(std::string line)
{
	if (!line.find("push int8("))
		return (verify_if_digit(Int8, line) ? commands::push8 : commands::NONE);
	else if (!line.find("push int16("))
		return (verify_if_digit(Int16, line) ? commands::push16 : commands::NONE);
	else if (!line.find("push int32("))
		return (verify_if_digit(Int32, line) ? commands::push32 : commands::NONE);
	else if (!line.find("push float("))
		return (verify_if_digit(Float, line) ? commands::pushFloat : commands::NONE);
	else if (!line.find("push double("))
		return (verify_if_digit(Double, line) ? commands::pushDouble : commands::NONE);
	return (commands::NONE);
}

commands	cmp_valid_command(std::string line)
{
	if (!line.find("push"))
		return (test_push(line));
	else if (!line.find("pop"))
		return (commands::pop);
	else if (!line.find("dump"))
		return (commands::dump);
	else if (!line.find("assert"))
		return (test_assert(line));
	else if (!line.find("add"))
		return (commands::add);
	else if (!line.find("sub"))
		return (commands::sub);
	else if (!line.find("mul"))
		return (commands::mul);
	else if (!line.find("div"))
		return (commands::div);
	else if (!line.find("mod"))
		return (commands::mod);
	else if (!line.find("print"))
		return (commands::print);
	else if (!line.find("exit"))
		return (commands::exit);
	return (commands::NONE);
}

void	ProcessLine::process_line(std::string line)
{
	std::string useable_line = line.substr(0, line.find(";"));
	if (useable_line.length() > 0)
	{
		commands cmd = cmp_valid_command(useable_line);
		std::string value;
		if (((cmd >= (commands)0) && (cmd <= (commands)4)) || ((cmd >= (commands)7) && (cmd <= (commands)11)))
			value = std::string(std::find(useable_line.begin(), useable_line.end(), '(') + 1, std::find(useable_line.begin(), useable_line.end(), ')'));
		else
			value.empty();
		switch (cmd)
		{
			case commands::push8:
				cal.addToStack(commands::push8, value);
				break ;
			case commands::push16:
				cal.addToStack(commands::push16, value);
				break ;
			case commands::push32:
				cal.addToStack(commands::push32, value);
				break ;
			case commands::pushDouble:
				cal.addToStack(commands::pushDouble, value);
				break ;
			case commands::pushFloat:
				cal.addToStack(commands::pushFloat, value);
				break ;
			case commands::pop:
				cal.popFromStack();
				break ;
			case commands::dump:
				cal.dumpCommand();
				break ;
			case commands::assert8:
				cal.assertCommand(Int8, value);
				break ;
			case commands::assert16:
				cal.assertCommand(Int16, value);
				break ;
			case commands::assert32:
				cal.assertCommand(Int32, value);
				break ;
			case commands::assertDouble:
				cal.assertCommand(Double, value);
				break ;
			case commands::assertFloat:
				cal.assertCommand(Float, value);
				break ;
			case commands::add:
				cal.addOperator();
				break ;
			case commands::sub:
				cal.subOperator();
				break ;
			case commands::mul:
				cal.mulOperator();
				break ;
			case commands::div:
				cal.divOperator();
				break ;
			case commands::mod:
				cal.modOperator();
				break ;
			case commands::print:
				cal.printCommand();
				break ;
			case commands::exit:
				break ;
			case commands::NONE:
				throw Exceptions::UnknownInstruction();
				break ;
		}
	}
}
