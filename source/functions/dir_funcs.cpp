/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   dir_funcs.cpp                                      :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: cnolte <cnolte@42.fr>                      +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2018/07/05 16:11:10 by cnolte            #+#    #+#             */
/*   Updated: 2018/07/05 16:25:47 by cnolte           ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "avm.header.hpp"
#include <dirent.h>

std::string	get_avm_file(int selection)
{
	DIR				*dir;
	struct dirent	*ent;
	int	count = 1;

	if ((dir = opendir (".")) != NULL)
	{
		while ((ent = readdir (dir)) != NULL)
		{
			if (hasEnding(ent->d_name, ".avm"))
			{
				if (count == selection)
					return (ent->d_name);
				else
					count++;
			}
		}
		closedir (dir);
	}
	else
	{
		perror ("[ERROR]");
		return (NULL);
	}
	return ("[ERROR]: Invalid selection.");
}

bool	print_avm_files(void)
{
	DIR				*dir;
	struct dirent	*ent;
	int	count = 1;

	if ((dir = opendir (".")) != NULL)
	{
		while ((ent = readdir (dir)) != NULL)
		{
			if (hasEnding(ent->d_name, ".avm"))
				std::cout << count++ << ": " << ent->d_name << std::endl;
		}
		closedir (dir);
	}
	else
	{
		perror("[ERROR]");
		return (false);
	}
	return (false);
}
