/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   overUnderFlowChecks.cpp                            :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: cnolte <cnolte@42.fr>                      +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2018/07/20 16:36:25 by cnolte            #+#    #+#             */
/*   Updated: 2018/07/21 18:37:54 by cnolte           ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "avm.header.hpp"

bool	testOverUnderFlow(eOperandType type, double d)
{
	if (std::isnan(d) == true || std::isinf(d) == true)
		throw Exceptions::ConversionNotPossible();
	switch(type)
	{
		case Int8:
			if (d > static_cast<double>(CHAR_MAX))
				throw Exceptions::ValueOverflow();
			if (d < static_cast<double>(CHAR_MIN))
				throw Exceptions::ValueUnderflow();
			break ;
		case Int16:
			if (d > static_cast<double>(SHRT_MAX))
				throw Exceptions::ValueOverflow();
			if (d < static_cast<double>(SHRT_MIN))
				throw Exceptions::ValueUnderflow();
			break ;
		case Int32:
			if (d > static_cast<double>(INT_MAX))
				throw Exceptions::ValueOverflow();
			if (d < static_cast<double>(INT_MIN))
				throw Exceptions::ValueUnderflow();
			break ;
		case Float:
			if (d > static_cast<double>(__FLT_MAX__))
				throw Exceptions::ValueOverflow();
			if (d < static_cast<double>(__FLT_MIN__))
				throw Exceptions::ValueUnderflow();
			break ;
		case Double:
			if (d > static_cast<double>(__DBL_MAX__))
				throw Exceptions::ValueOverflow();
			if (d < static_cast<double>(__DBL_MIN__))
				throw Exceptions::ValueUnderflow();
			break ;
		default:
			break ;
	}
	return (true);
}
