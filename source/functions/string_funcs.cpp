/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   string_func.cpp                                    :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: cnolte <cnolte@42.fr>                      +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2018/07/05 16:04:32 by cnolte            #+#    #+#             */
/*   Updated: 2018/07/05 16:07:05 by cnolte           ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "avm.header.hpp"

std::string	str_to_lower(std::string str)
{
	for(unsigned int i = 0; i < str.length(); ++i)
		str[i] = tolower(str[i]);
	return (str);
}

char	*char_str_to_lower(char *str)
{
	int		i;
	int		len;

	i = 0;
	len = std::strlen(str);
	while (i < len)
	{
		str[i] = std::tolower(str[i]);
		i++;
	}
	return (str);
}

bool	hasEnding(std::string const &fullString, std::string const &ending)
{
	if (fullString.length() >= ending.length())
	{
		int	pos = fullString.length() - ending.length();
		
		if (fullString.compare(pos, ending.length(), ending) == 0)
			return (true);
		else
			return (false);
	}
	else
		return (false);
	return (false);
}
