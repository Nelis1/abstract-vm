/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   main.cpp                                           :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: cnolte <cnolte@42.fr>                      +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2018/06/11 11:31:55 by cnolte            #+#    #+#             */
/*   Updated: 2018/07/21 15:27:46 by cnolte           ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "avm.header.hpp"
#include <fstream>

int	main(int argc, char **argv)
{
	ProcessLine processline;

	if (argc == 2)
	{
		if (std::strcmp("all", char_str_to_lower(argv[1])) == 0)
		{
			print_avm_files();
			int selection = 0;
			std::cin >> selection;
			if (selection > 0)
			{
				try
				{
					process_file(get_avm_file(selection), processline);
				}
				catch (std::exception &e)
				{
					std::cerr << "[ERROR]: " << e.what() << std::endl;
				}
			}
			else
				std::cerr << "[ERROR]: No valid selection made." << std::endl;
		}
		else if (std::strcmp("realtime", char_str_to_lower(argv[1])) == 0)
		{
			char	charRead[256];

			while (str_to_lower(charRead) != "exit")
			{
				std::cin.getline(charRead, 256);
				try
				{
					processline.process_line(charRead);
				}
				catch (std::exception &e)
				{
					std::cerr << "[ERROR]: " << e.what() << std::endl;
				}
			}
			
		}
		else
			try
			{
				process_file(argv[1], processline);
			}
			catch (std::exception &e)
			{
				std::cerr << "[ERROR]: " << e.what() << std::endl;
			}
	}
	else
	{
		char			charRead[256];
		std::ofstream	myfile;
		myfile.open ("input.tmp");
		while (str_to_lower(charRead) != "exit")
		{
			std::cin.getline(charRead, 256);
			myfile << charRead << std::endl;
		}
		myfile.close();
		while (true)
		{
			try
			{
				std::cin.getline(charRead, 256);
				if (std::strcmp(";;", char_str_to_lower(charRead)) == 0)
				{
					try
					{
						process_file("input.tmp", processline);
						break ;
					}
					catch (std::exception &e)
					{
						std::cerr << "[ERROR]: " << e.what() << std::endl;
						break ;
					}
				}
				else
					throw Exceptions::UnknownInstruction();
			}
			catch (std::exception &e)
			{
				std::cerr << "[ERROR]: " << e.what() << std::endl;
				std::cerr << "Expecting \";;\" command. Try again..." << std::endl;
			}
		}
		std::cout << std::endl;
		while (true)
		{
			std::cout << "Want to delete temporary file of standard input?" << std::endl;
			std::cout << "\"y\" for yes | \"n\" for no." << std::endl;
			std::cin.getline(charRead, 256);
			try
			{
				if (str_to_lower(charRead) == "y")
				{
					remove("input.tmp");
					break ;
				}
				else if (str_to_lower(charRead) == "n")
					break ;
				else
				{
					throw Exceptions::UnknownInstruction();
				}
			}
			catch (std::exception &e)
			{
				std::cerr << std::endl << "[ERROR]: " << e.what() << std::endl;
			}
		}
		
	}
	return (0);
}
