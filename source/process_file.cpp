/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   process_file.cpp                                   :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: cnolte <cnolte@42.fr>                      +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2018/07/05 15:47:53 by cnolte            #+#    #+#             */
/*   Updated: 2018/07/21 18:33:51 by cnolte           ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "avm.header.hpp"
#include <fstream>

void	process_file(std::string filename, ProcessLine process_line)
{
	std::ifstream	inFile;
	std::string		line;
	bool			exit_found = false;

	inFile.open(filename);
	if (!inFile)
		throw Exceptions::FileDoesNotExist(filename);
	else
	{
		while (std::getline(inFile, line))
		{
			if (line.find("exit") == 0)
			{
				exit_found = true;
				break ;
			}
			process_line.process_line(line);
		}
		inFile.close();
		if (exit_found == false)
			throw Exceptions::NoExit();
	}
}
